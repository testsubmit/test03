using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// 아이템 상점(아래 하단)
/// </summary>
public class Store : MonoBehaviour
{
    /// <summary>
    /// 인벤토리 매니저를 받아옴
    /// </summary>
    public InventoryManager inventoryManager;
    
    /// <summary>
    /// 아이템 픽업 배열
    /// </summary>
    public Item[] itemsToPickUp;

    /// <summary>
    /// 아이템을 가져올 클래스
    /// </summary>
    /// <param name="id">해당 아이템의 개별 아이디를 받아와서 출력</param>
    public void PickUpItem(int id)
    {
        // 인벤토리 매니저를 통해 아이템을 추가하고 저장 가능 여부 결과 저장
        bool result = inventoryManager.AddItem(itemsToPickUp[id]);

        //만약 들어갈 수 있다면 
        if (result == true)
        {
            //아이템 채워짐 메시지 출력
            Debug.Log("아이템 채워짐");
        }
        //꽉 찬다면 더 들어가지 않음 메시지 출력
        else Debug.Log("아이템 꽉참");
    }

}
