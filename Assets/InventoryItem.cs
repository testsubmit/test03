using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal.Profiling.Memory.Experimental;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// 인벤토리 아이템을 나타내며 아이템의 드래그 이동을 구현
/// </summary>
public class InventoryItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    /// <summary>
    /// 아이템 정보를 담고 있는 스크립터블 오브젝트
    /// </summary>
    public Item item;

    /// <summary>
    /// 아이템 출력용 이미지
    /// </summary>
    [Header("UI")]
    public Image image;

    /// <summary>
    /// 드래그 이동 후 아이템의 부모를 저장하는 변수, 인스펙터에서는 숨김
    /// </summary>
    [HideInInspector]
    public Transform parentAfterDrag;

    public void Start()
    {
        // 아이템 초기화
        InitItem(item);
    }

    /// <summary>
    /// 아이템을 초기화
    /// </summary>
    /// <param name="newItem">초기화할 아이템</param>
    public void InitItem(Item newItem)
    {
        //아이템 정보 받아옴
        item = newItem;

        //아이템 스프라이트 이미지 생성
        image.sprite = newItem.m_ItemSprite;
    }

    /// <summary>
    /// 드래그 이동 시작용 메소드
    /// </summary>
    /// <param name="eventData"></param>
    public void OnBeginDrag(PointerEventData eventData)
    {
        //Debug.Log("이동 시작");

        //드래그 이동 이전의 부모를 저장
        parentAfterDrag = transform.parent;

        //드래그 중에는 아이템이 최상위에 표시되도록 부모를 루트로 설정 : 어떤 부모도 결정되지 않도록
        transform.SetParent(transform.root);

        //다른 부모 오브젝트와 겹치지 않도록 최상위로 이동시킴
        transform.SetAsLastSibling();

        //이미지의 레이캐스트 타겟 비활성화
        image.raycastTarget = false;
    }

    /// <summary>
    /// 드래그 이동 중 메소드
    /// </summary>
    /// <param name="eventData"></param>
    public void OnDrag(PointerEventData eventData)
    {
        //Debug.Log("이동 중");

        //아이템을 마우스 드래그 위치로 이동
        transform.position = Input.mousePosition;
    }
    
    /// <summary>
    /// 드래그 이동 종료용 메소드
    /// </summary>
    /// <param name="eventData"></param>
    public void OnEndDrag(PointerEventData eventData)
    {
        //Debug.Log("이동 종료");

        //이동 후의 부모로 설정
        transform.SetParent(parentAfterDrag);

        //이미지의 레이캐스트 타겟 활성화
        image.raycastTarget = true; 
    }


}
