using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

/// <summary>
/// InventoryManager 클래스, 아이템을 실질적으로 관리
/// </summary>
public class InventoryManager : MonoBehaviour
{
    /// <summary>
    /// 아이템이 실제 들어가게 될 배열
    /// </summary>
    public InventorySlot[] InventorySlots;

    /// <summary>
    /// 아이템을 실제 위치시키는 인벤토리 아이템 프리팹
    /// </summary>
    public GameObject InventoryItemPrefabs;

    /// <summary>
    /// 아이템을 추가하는 AddItem 메서드를 정의
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public bool AddItem(Item item)
    {
        // InventorySlots 배열의 길이만큼 반복, 인벤토리 슬롯의 모든 칸 검사
        for (int i = 0; i < InventorySlots.Length; i++) 
        {
            //해당 슬롯 위치 지정
            InventorySlot slot = InventorySlots[i];

            //현재 InventorySlot의 자식 객체에서 InventoryItem 컴포넌트를 가져옴
            InventoryItem itemInSlot = slot.GetComponentInChildren<InventoryItem>();    

            //만약 아이템 슬롯이 비어있다면
            if(itemInSlot == null)
            {
                //아이템을 슬롯에 추가
                spawnNewItem(item, slot);
                
                //아이템 들어갈 수 있음
                return true;
            }
        }

        //아이템 들어갈 수 없음
        return false;
    }

    /// <summary>
    /// 새로운 아이템을 생성하는 spawnNewItem 메서드 정의
    /// </summary>
    /// <param name="item"></param>
    /// <param name="slot"></param>
    void spawnNewItem(Item item, InventorySlot slot)
    {
        //인벤토리 아이템 프리팹을 슬롯의 위치에 생성
        GameObject newItemGo = Instantiate(InventoryItemPrefabs, slot.transform);

        //새로 생성된 아이템의 InventoryItem 컴포넌트를 가져옴
        InventoryItem inventoryItem = newItemGo.GetComponent<InventoryItem>();

        //아이템 생성
        inventoryItem.InitItem(item); 
    }
}
