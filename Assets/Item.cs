using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

/// <summary>
/// 아이템 정보를 정의하기 위한 ScriptableObject 클래스입니다.
/// </summary>
[CreateAssetMenu(fileName = "Item", menuName = "Scriptable Object", order = int.MaxValue)]
public class Item : ScriptableObject
{
    [Header("# 아이템 코드")]
    public int m_ItemCode;

    [Header("# 아이템 이미지")]
    public Sprite m_ItemSprite;

    [Header("# 아이템 이름")]
    public string m_ItemName;   

}


