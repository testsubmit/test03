using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// 드래그 앤 드롭 이벤트를 처리할 수 있는 인벤토리 슬롯
/// </summary>
public class InventorySlot : MonoBehaviour, IDropHandler
{
    /// <summary>
    /// 이 인벤토리 슬롯에 아이템이 드롭될 때 호출
    /// </summary>
    /// <param name="eventData">드롭 이벤트와 관련된 PointerEventData</param>
    public void OnDrop(PointerEventData eventData)
    {
        //하위에 자식오브젝트가 없다면 : 자식오브젝트가 있다면 들어가지 않음
        if(transform.childCount == 0)
        {
            //드롭된 게임 오브젝트를 가져옴
            GameObject dropped = eventData.pointerDrag;

            //드롭된 오브젝트에서 InventoryItem 컴포넌트를 가져옴
            InventoryItem draggableItem = dropped.GetComponent<InventoryItem>();

            // 드래그된 아이템의 부모를 이 슬롯으로 설정
            draggableItem.parentAfterDrag = transform;
        }
    }
}
