using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Unity.VisualScripting;

/// <summary>
/// 버튼 우클릭용 스크립트
/// </summary>
public class ClickableObject : MonoBehaviour, IPointerClickHandler
{
    /// <summary>
    /// 버튼 우클릭용 특별 클래스
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClick(PointerEventData eventData)
    {        
        //해당하는 버튼에 우클릭이 들어왔다면
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            //우클릭 메시지 출력
            Debug.Log("우클릭 : 아이템 제거됨");
            
            //해당하는 버튼 하위에 있는 InventoryItem 게임오브젝트 제거
            Destroy(gameObject.GetComponentInChildren<InventoryItem>().gameObject);
        }
    }
}